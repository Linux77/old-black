O gerenciador de inicialização grub, é o padrão de muitos sistemas.
Ele foi um dos primeiros a dar suporte a tecnologias mais recentes.
No caso de restaurações de gerenciadores de inicialização, pode-se:
Em ambiente efi:
Inicializar a partir de uma mídia qualquere criar o ambiente chroot.
A partir disso poderá reinstalar o gerenciador de forma correta.
Antes de entrar no ambiente chroot do sistema a ser recuperado 
não se esqueça de carregar o módulo do sistema de arquivos efi
no kernel de inicialização atual. Para isso:
modprobe efivars
após isso vc poderá entrar no ambiente com chroot 
e reinstalar o grub de forma correta.
