# old-black.

![logo](img/logo-s.png "old-black")

Este projeto e software possui várias formas de uso. Ele surgiu como um robô para chat usando o protocolo IRC.

O projeto está sendo desenvolvido em **PERL**.

Depois de algum tempo em testes, surgiu a idéia de otimizar seu código e implementar novas funcionalidades.

Em um segundo momento o aplicativo foi adquirindo novos recursos como:

* permitir o cadastro de novas palavras chave, definições e conteúdos.
* ser usado em conjunto com outro script que compõe o projeto o old-cad.


---

[**Online version**](index.md)

## Instalando o projeto

Em ambientes debian e derivados suas depêndencias são:
* espeak
Ele é o sintetizador de voz.

* mbrola-br1

Voz para o _português Brasil_.

* Instalando as dependências:

Com o super usuário:

apt install espeak mbrola-br1

Com usuário pertencente ao grupo _sudo_:

sudo apt install espeak mbrola-br1

Após isso basta clonar o projeto de seu repositório usando a ferramente git:

#~git clone https://codeberg.org/Linux77/old-black.git

---

**Uma de suas aplicações é**:

**Instrutor virtual**.

**old-black**
> Por padrão o aplicativo cria um instrutor virtual com 19 aulas.


> Estas tem um conteúdo voltado para ciência da computação e tecnologia da informação  de forma geral.

> Este aplicativo tenta auxiliar portadores de deficiência visual.
Tendo em seu modo de execução pelo console de um sistema linux a opção de saída de dados utilizando um sintetizador de voz.

## Conhecendo e dando início ao uso do projeto ##

Uma vez clonado via git o projeto para seu lugar de destino e execução, pode-se dar inínio ao seu uso, conhecendo o seu funcionamento da seguinte forma:

O script **old-black**:

#~./old-black ajuda

Aqui executamos o script passando para ele  a palavra chave "ajuda" como um parêmetro.

Ele irá retornar as opções básicas de seu funcionamento.

O script old-black constrói a interface para saída e consulta de dados.
Ele uma vez executado encontrando uma definição para a palavra chave, exibe essa definição, e caso haja algum texto referente a ela, permite a leitura e(ou) narração do mesmo.

Ele possui uma interface no modo texto.

Tendo as opções para saídade de dados em textos ou com sintetizador de voz, o que auxilía portadores de deficiências visuais.

>Foi desenvolvido em sistema debian e creio ter compatibilidade com seus derivados.

---

**Outras finalidades**:


**Sistema de anotações e estruturas de estudo**.

Outro script que compõe o projeto **old-cad**:

> Este aplicativo cria entradas de palavras chave e definições nos arquivos usados pelo old-black.


> O projeto old-black tenta auxiliar portadores de deficiência visual.


## Cadastrando novos conteúdos
No projeto temos dois scripts executáveis até o momento.

O script **old-cad**:

Este script é usado de forma bem simples, algo como:

#~./old-cad

Ele irá realizar perguntas sobre a nova palavra chave a ser cadastrada e sua definição.

Uma vez adicionada a palavra chave, o script old-black será capaz de localizar e exebit sua definição.

Caso se tenha vontade de associar um novo texto a essa nova palavra chave cadastrada, basta adicionar um arquivo no formato texto, onde:

O nome do arquivo seja a nova palavra chave e sua extensão ".txt".

Com esse método para o cadastro das palavras chave, definição e novos conteúdos, o script old-black além de exibir a definição, permitirá a saída do texto para leitura e (ou) narração.

---

[**Website for my courses and studies:**](http://www.asl-sl.com.br)
> Here I provide some studies, projects related to tecnology. I teach in my small city during some time an>

> On my courses and studies **website** I use __portuguese,__ my natural language.


[**Website search engine projects:**](http://magicbyte.tec.br:8888/)


> Here I install one public seaech __engine__ for studies end learning purposes.

> The software used, also promotes one metasearch, and small crawler.

> You are welcome to known and colaborate.


_contact_**/contato:**

### Leonardo.

> Apaixonado por tecnologia, professor com graduação em ensino fundamental e médio.

> Desde menino ainda na década de 80 sempre acompanhando evoluções em telecomunicações.

> Radio amador quando jovem, ainda me lembro de antever tecnologias, em nossos ideiais.

> Programador com experiência em linguagens de alto e baixo nível....

> Tive influências de conhecidos e parentes dobre questões de conhcer ambientes na época distantes da maior parte das pessoas.

> ainda me recordo de uma _tia_, programadora de ambientes UNIX, na época de locação de mainframes, me despertou o interesse por me aprofundar em tais estudos.

> Muitos anos depois tive meus primeiros contatos com ambiente BSD's e Linux o que despertou uma paixão adormecida.

> Hoje tenho mais de 20 anos de ensino técnico em uma estrutura prórpia, a qual em minha pequena cidade e região mostrou eficácia, mudando a vida de muitos adolescentees hoje profissionais espealhados por várias localidades.



**fone:** (35) 99120-5702.

> [email](mailto:leonardo@asl-sl.com.br)

> **Linux77.**

### Mônica.

**fone:** (35) 99853-7574.

**email:** [Mônica](mailto:monijucodoro@gmail.com)

[Academia do software livre **Brasil - MG**](http://www.asl-sl.com.br)

[Courses and studies**](http://www.cursos.asl-sl.com.br)

[Um pouco sobre nossa cidade!](http://www.asl-br.com/TMB)

[A short about our City!](http://www.asl-br.com/TMB)

[contact](mailto:feraleomg@gmail.com)
